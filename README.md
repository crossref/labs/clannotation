# CLAnnotator: Crossref Labs Annotator 
CLAnnotator is the module that Crossref Labs uses to decorate the experimental API.

![license](https://img.shields.io/gitlab/license/crossref/labs/clannotation) ![activity](https://img.shields.io/gitlab/last-commit/crossref/labs/clannotation)

![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white) ![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)

This is a prototype Crossref Labs system. It is not guaranteed to be stable and the metadata schema and behaviour may be subject to change at any time.

## Installation

To install the CLAnnatator module, simply use the following pip command:

    pip install clannatation

## Notes
ISSN identifiers are normalized to remove hyphens and to capitalize the final X where it occurs.

## License
CLAnnotator is released under the [MIT License](https://opensource.org/licenses/MIT).

&copy; Crossref 2023