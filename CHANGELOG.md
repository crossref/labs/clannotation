# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.17] - 2024-01-15
* Handle failed annotation better

## [0.0.14] - 2023-12-19
* Adjustments to route rebuild in journals route

## [0.0.13] - 2023-12-18
* Further fixes

## [0.0.12] - 2023-12-18
* Further fixes

## [0.0.11] - 2023-12-18
* Fix to bug in all-journals route

## [0.0.10] - 2023-11-22
* Refine journal annotation behaviour

## [0.0.9] - 2023-11-22
* Fix journal annotations

## [0.0.8] - 2023-07-12
* Fix bug in works annotation

## [0.0.7] - 2023-06-12
* Update CLAWS dependency version

## [0.0.6] - 2023-05-10
* Update CLAWS dependency version

## [0.0.5] - 2023-04-25
* Update longsight version

## [0.0.4] - 2023-04-25
* Update longsight version

## [0.0.3] - 2023-04-20
* Bugfix: 0.0.2 broke non-instrumented annotation

## [0.0.2] - 2023-04-20
* Fix logging if no instrumentation

## [0.0.1] - 2023-04-20
* First release
